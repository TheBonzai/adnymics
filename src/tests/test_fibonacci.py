import pytest
from server.services.fibonacci_combinations import get_fibonacci_combinations, CombinationsCache
from server.services.fibonacci_sequence import get_fibonacci_sequence, reset_cache as reset_sequence_cache
from tests.helper import deep_sort


class TestFibonacciSequence:
    @pytest.mark.parametrize("value", [1, -10])
    def test_invalid_value(self, value):
        assert get_fibonacci_sequence(value) == []

    @pytest.mark.parametrize("value,expected", [
        (2, [2]),
        (3, [2, 3]),
        (12, [2, 3, 5, 8]),
        (13, [2, 3, 5, 8, 13]),
        (21, [2, 3, 5, 8, 13, 21])
    ])
    def test_calculation(self, value, expected):
        assert get_fibonacci_sequence(value) == expected


class TestFibonacciCombinations:
    def setup_method(self):
        CombinationsCache.reset()
        reset_sequence_cache()

    @pytest.mark.parametrize("value,expected", [
        (3, [[3]]),
        (5, [[2, 3], [5]]),
        (11, [
            [2, 2, 2, 2, 3],
            [2, 3, 3, 3],
            [2, 2, 2, 5],
            [3, 3, 5],
            [3, 8]
        ]),
        (15, [
            [2, 2, 2, 2, 2, 2, 3],
            [2, 2, 2, 2, 2, 5],
            [2, 2, 2, 3, 3, 3],
            [3, 3, 3, 3, 3],
            [2, 2, 3, 3, 5],
            [5, 5, 5],
            [2, 3, 5, 5],
            [2, 2, 3, 8],
            [2, 5, 8],
            [2, 13],
        ])
    ])
    def test_valid_input(self, value, expected):
        assert deep_sort(get_fibonacci_combinations(value)) == deep_sort(expected)
