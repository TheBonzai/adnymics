import pytest
import server
import json
from tests.helper import deep_sort


@pytest.fixture
def client():
    server.app.config['TESTING'] = True

    with server.app.test_client() as client:
        yield client


class TestFibonacci:
    def test_valid_input(self, client):
        data = client.get('/fib/11').data
        assert deep_sort(json.loads(data)) == deep_sort(
            [[2, 2, 2, 2, 3], [2, 3, 3, 3], [2, 2, 2, 5], [3, 3, 5], [3, 8]])

    def test_invalid_input(self, client):
        data = client.get('/fib/-1').data
        assert data.decode() == "Please provide a positive integer"


class TestHealth:
    def test_health(self, client):
        data = client.get('/health').data
        assert json.loads(data)["status"] == "success"
