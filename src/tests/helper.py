from typing import List


def deep_sort(arr: List[List[int]], reverse: bool = True) -> List[List[int]]:
    return sorted([sorted(x, reverse=reverse) for x in arr])
