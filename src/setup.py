import setuptools
import server.config as config

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="adnymics-task",
    version=config.VERSION,
    author="William Ma",
    author_email="william.ma@rwth-aachen.de",
    description="Task 2",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/TheBonzai/adnymics/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
