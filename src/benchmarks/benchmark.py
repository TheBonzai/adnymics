import perfplot
from server.services.fibonacci_combinations import get_fibonacci_combinations, CombinationsCache

name = "test"


def setup(n):
    CombinationsCache.reset()
    return n


perfplot.save(
    name.lower().replace(" ", "_"),
    setup=setup,
    kernels=[
        lambda x: print(x),
    ],
    n_range=[1, 2, 3],
    xlabel="n",
    title=name,
    logx=False,
    logy=True,
    equality_check=None,  # set to None to disable "correctness" assertion
    time_unit="s",  # set to one of ("auto", "s", "ms", "us", or "ns") to force plot units
)
