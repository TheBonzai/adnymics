from server import app
from flask import jsonify
from server.services.fibonacci_combinations import get_fibonacci_combinations


@app.route("/fib/<number>")
def fibonacci(number: str):
    if not number.isdigit():
        return "Please provide a positive integer", 400

    return jsonify(get_fibonacci_combinations(int(number))), 200
