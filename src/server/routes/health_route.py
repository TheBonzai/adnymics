from server import app
from server.services.health import get_health_status


@app.route("/health")
def health():
    return get_health_status()
