from typing import List
from bisect import bisect_right

_cache: List[int] = [2, 3]


def _find_edge_less_or_equal(max_value: int) -> int:
    return bisect_right(_cache, max_value)


def _extend_sequence_cache(min_value: int):
    if _cache[-1] < min_value:
        _cache.extend(_calc_fibonacci_sequence(min_value, _cache[-1], _cache[-2]))


def _calc_fibonacci_sequence(min_number: int, current_fibonacci: int = 2, last_fibonacci: int = 1) -> List[int]:
    result = []
    current, last = current_fibonacci, last_fibonacci
    while current < min_number:
        current, last = current + last, current
        result.append(current)

    return result


def get_fibonacci_sequence(max_value: int) -> List[int]:
    _extend_sequence_cache(max_value)
    index = _find_edge_less_or_equal(max_value)
    return _cache[:index]


def reset_cache():
    _cache = [2, 3]


if __name__ == "__main__":
    fib_seq = get_fibonacci_sequence(10)
    print(fib_seq)
