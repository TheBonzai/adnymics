from healthcheck import HealthCheck
from server.__version__ import __version__

health = HealthCheck()

health.add_section("application", "Fibonacci sum combinations finder")
health.add_section("version", __version__)
health.add_section("description",
                   "Health status of fibonacci sum combinations service")


def get_health_status():
    return health.run()
