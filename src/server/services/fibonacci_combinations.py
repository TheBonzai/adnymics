from typing import List, Dict
from server.services.fibonacci_sequence import get_fibonacci_sequence

from server.utils.decorators.argument_call_counter import argument_call_counter


class CombinationsCache:
    _combinations: Dict[int, List[List[int]]] = {}

    @staticmethod
    def get_combinations(number: int) -> List[List[int]]:
        res = CombinationsCache._combinations.get(number)
        return [[*r] for r in res] if res else None

    @staticmethod
    def set_combinations(number: int, combinations: List[List[int]]):
        CombinationsCache._combinations[number] = [[*c] for c in combinations]

    @staticmethod
    def reset():
        CombinationsCache._combinations = {}



# @argument_call_counter(group_by_nth=0)
def get_fibonacci_combinations(number: int) -> List[List[int]]:
    res = CombinationsCache.get_combinations(number)
    if res:
        return res

    fibonacci_seq = get_fibonacci_sequence(number)

    res = []

    for idx, fibonacci in enumerate(fibonacci_seq):
        next_num = number - fibonacci

        if next_num < 0:
            continue
        elif next_num == 0:
            res.append([number])
        else:
            combs = get_fibonacci_combinations(next_num)
            for comb in combs:
                if fibonacci <= comb[-1]:
                    comb.extend([fibonacci])
                    res.append(comb)

    CombinationsCache.set_combinations(number, res)
    return res


if __name__ == "__main__":
    fib_combs = get_fibonacci_combinations(180)
    print(len(fib_combs))

    # import matplotlib.pyplot as plt
    # calls = _calc_fibonacci_combinations.calls
    # c = sorted([(int(t[0]), t[1]) for t in calls.items()], key=lambda x: x[0])
    #
    # plt.grid()
    # plt.yscale("linear")
    # plt.bar(*zip(*c))
    # plt.title("Calls of _get_fibonacci_combinations()")
    # plt.xlabel("Argument: number")
    # plt.ylabel("# calls")
    # plt.savefig("num_calls_no_cache_lin.png")
