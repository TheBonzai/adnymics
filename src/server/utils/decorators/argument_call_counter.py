import json


def argument_call_counter(group_by_nth: int = 0):
    def decorator(func):
        def wrapper(*args):
            key = json.dumps(args[group_by_nth])
            if key not in wrapper.calls:
                wrapper.calls[key] = 0
            wrapper.calls[key] += 1
            return func(*args)

        wrapper.calls = {}
        return wrapper

    return decorator
