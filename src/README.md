# Adnymics - Developer Task

This is my solution to task 2. 
The Fibonacci combination calculator can be found in `server/services/fibonacci_combinations.py`. 
This calculator recursively finds the combinations and caches them.

## Comments

/Health api:

I would add some status information about Cassandra (response time, status of cluster)


> Write tests where it makes sense to you.

There are some tests in `/tests`. 

> Log the requests/responses being made to /fib in a database of your choice.

```
I would use Cassandra with a schema like this: 

CREATE TABLE IF NOT EXISTS requests
(
    // something like the date: YYYYMMDD, to keep partitions small
    time_bucket    text,            
    
    // maybe also store endpoint like "/fib" or "/health"
 // endpoint       text,            
    
    // store time and prevent collision when only storing timestamp
    request_id     timeuuid,        
    payload        text,
    
    // I think that here only metadata (like response code etc) not the combinations should be stored*
    response       int,             
    
    // partition by time_bucket, and cluster by request_id
    PRIMARY KEY ((time_bucket), request_id )    
) with CLUSTERING ORDER BY (request_id DESC);
```

If `endpoint` is needed, is must be included in the partition key.

*: The responses are rather large, therefore the partition size would grow very quickly.
Furthermore, there would be a lot of (large) duplications, 
therefore it should be normalized and store in an other way like a relational database.
When then the calculation results are needed, 
the request logs has to be joined manually with the data from the relational database. 
Besides, the combinations are very compressible.


> Caching layer to avoid recalculating of Fibonacci sequences.

The calculation of the Fibonacci sequences are rather quick compared to the calculation of the combinations. 
Therefore, the data size of the sequences are small compared to the data size of the calculations.
Optimization of the combinations is more important,
but if still the sequences has to be stored, 
I first thought about using redis because the sequences would fit into memory, 
but I have found no function to make a range query by value,
so wrote a small cache in `server/services/fibonacci_sequence.py`, which uses a binary search to find the required sequence.


> Basic benchmarking to see how you code preform.

There are some plots in `/benchmarks/trash` but I think that I have used the package `perfplot` incorrectly or it does strange things, since the times are wrong. 
If I had more time I would monitor CPU and memory usage. Also I would profile the algorithm to see the bottlenecks.


> Setup tox to test your project with several python versions.

This is just compatible with python3.8 (maybe python3.6) because I have used type annotations.

> Dockerize your service.

I have used docker-compsose. Actually I have planned to add a cassandra container but I had no time.
I have copied requirements.txt to the container folder manually, but it can be done with auto deploy
 
## Requirements
```
python >= 3.8.0
```
I just have used python3.8, 
but I assume that it is also compatible with python3.6+,
because I used type annotations. 
This module should be compatible with python2.7+ when I remove the annotations.

## Usage
Start a development server:
```
python wsgi.py
```


## Testing
```
pip install -r requirements.txt
pytest
```

Compatibility:
```
tox
```
