# Adnymics - Developer Task

Task 1 is in `TASK_1` file. Task 2 in `src/` (see `src/README.md`).

1. Copy `env.template` to `.env` (some configs can be made here)

2. Start with: 
```
sudo docker-compose up
```

